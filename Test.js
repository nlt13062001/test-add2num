class MyBigNumber {
    sum(stn1, stn2) {
        let result = "";
        let carry = 0;
        // độ dài chuỗi của 2 số
        let length1 = stn1.length;
        let length2 = stn2.length;
        // duyệt phải sang trái và lấy từng kí tự
        for (let i = 0; i < Math.max(length1, length2); i++) {
            let num1 = (i < length1) ? parseInt(stn1[length1 - 1 - i]) : 0;
            let num2 = (i < length2) ? parseInt(stn2[length2 - 1 - i]) : 0;
            // duyệt và cộng số
            let sum = num1 + num2 + carry;           
            result = (sum % 10) + result;
            carry = Math.floor(sum / 10);
        }
        if (carry > 0) {
            result = carry + result;
        }
        console.log(`Adding ${stn1} and ${stn2} results in ${result}`);
        return result;
    }
}
